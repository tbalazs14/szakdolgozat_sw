#define hall_b_Pin GPIO_PIN_2
#define hall_b_GPIO_Port GPIOA
#define hall_a_Pin GPIO_PIN_3
#define hall_a_GPIO_Port GPIOA
#define pwm_motor_Pin GPIO_PIN_5
#define pwm_motor_GPIO_Port GPIOA
#define n_sleep_motor_Pin GPIO_PIN_6
#define n_sleep_motor_GPIO_Port GPIOA
#define dir_motor_Pin GPIO_PIN_7
#define dir_motor_GPIO_Port GPIOA
#define slow_fast_brake_motor_Pin GPIO_PIN_0
#define slow_fast_brake_motor_GPIO_Port GPIOB
#define motor_current_Pin GPIO_PIN_1
#define motor_current_GPIO_Port GPIOB
#define sw_wake_motor_Pin GPIO_PIN_8
#define sw_wake_motor_GPIO_Port GPIOA
#define uart_tx_Pin GPIO_PIN_9
#define uart_tx_GPIO_Port GPIOA
#define uart_rx_Pin GPIO_PIN_10
#define uart_rx_GPIO_Port GPIOA
#define hw_wake_motor_Pin GPIO_PIN_11
#define hw_wake_motor_GPIO_Port GPIOA
#define int_mag_Pin GPIO_PIN_12
#define int_mag_GPIO_Port GPIOA
#define swdio_Pin GPIO_PIN_13
#define swdio_GPIO_Port GPIOA
#define swclk_Pin GPIO_PIN_14
#define swclk_GPIO_Port GPIOA
#define gomb3_Pin GPIO_PIN_15
#define gomb3_GPIO_Port GPIOA
#define gomb2_Pin GPIO_PIN_3
#define gomb2_GPIO_Port GPIOB
#define gomb1_Pin GPIO_PIN_4
#define gomb1_GPIO_Port GPIOB
#define i2c_scl_Pin GPIO_PIN_6
#define i2c_scl_GPIO_Port GPIOB
#define i2c_sda_Pin GPIO_PIN_7
#define i2c_sda_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define period_value 1600
#define magic_number 23080275.7
#define RPM_to_freq 0.677255
#define sinus(x) (x-x*x*x/6 + x*x*x*x*x/120-x*x*x*x*x*x*x/5040)
#define max_RPM 234
#define pi 3.141592654
/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
