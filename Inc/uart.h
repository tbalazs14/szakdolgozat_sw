#ifndef INC_UART_H_
#define INC_UART_H_

typedef struct
{
	uint8_t size;
	const char* command;
}BT_Command;

 struct rx_process_state_machine
{
	uint8_t rx_state;
	uint8_t process_rx_buffer_start;
	uint8_t process_rx_buffer_index;
	uint8_t rx_handle[4];
};


void UART_Init();
void init_RN4020();
void set_baudrate(uint32_t br);
void write_to_characteristic(uint8_t* UUID, uint8_t* value, uint8_t bytes);
void write_to_hex_string(uint8_t* values,uint8_t bytes, uint8_t* values_in_hex);
void read_from_characteristic(uint8_t* UUID, uint8_t* value, uint8_t bytes);
void read_from_hex_string(uint8_t* values_in_hex,uint8_t characters,uint8_t* values);
void list_server_services();
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);
void process_rx_buffer();
#endif /* INC_UART_H_ */
