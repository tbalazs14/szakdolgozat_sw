#ifndef INC_ADC_H_
#define INC_ADC_H_

void ADC_Init();
void ADC_IRQHandler(void);
void StartConversion();
uint16_t ADC_Get_Value(uint32_t);
uint32_t ADC_Get_Voltage(uint32_t);

#endif /* INC_ADC_H_ */
