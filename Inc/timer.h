#ifndef INC_TIMER_H_
#define INC_TIMER_H_

void Timer2_Init();
void Timer21_Init();
void Timer22_Init();
void start_motor();
void stop_motor();
void SetPWM(uint16_t d);
void TIM21_IRQHandler();
void TIM22_IRQHandler();
void Timer22Handler();

void SetMotor(float RPM);

#endif /* INC_TIMER_H_ */
