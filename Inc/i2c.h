#ifndef INC_I2C_H_
#define INC_I2C_H_

#define I2C_TIMING 0x20412B38 //Analog filter on, rise time 150ns, fall time 150ns
#define I2C_ADDRESS_MPU_9250 0xD0 //7 bit address: 0b01101000->0b11010000 = 0xD0

#define MPU_9250_SMPLRT_DIV 0x19
#define MPU_9250_CONFIG 0x1A
#define MPU_9250_GYRO_CONFIG 0x1B
#define MPU_9250_ACC_CONFIG 0x1C
#define MPU_9250_ACC_CONFIG_2 0x1D
#define MPU_9250_FIFO_ENABLE 0x23
#define MPU_9250_I2C_MST_CTRL 0x24
#define MPU_9250_INT_PIN_CFG 0x37
#define MPU_9250_INT_ENABLE 0x38
#define MPU_9250_INT_STATUS 0x3A //read-only
#define MPU_9250_ACCEL_XOUT_H 0x3B
#define MPU_9250_WHO_AM_I 0x75
#define MPU_9250_USER_CTRL 0x6A
#define MPU_9250_PWR_MGMT1 0x6B
#define MPU_9250_PWR_MGMT2 0x6C

#define I2C_ADDRESS_AK8963  0x18 //0x0c or 0x0d, 0x0e, 0x0f according to cad1/0 pins->0x18,0x1a,0x1c,0x1e
//7 bit address: 0x0c = 0b00001100->0b00011000 = 0x18

#define AK8963_WHO_AM_I 0x00 //value is fixed: 0x48
#define AK8963_ST1 0x02 //status register 1
#define AK8963_ST2 0x09 //status register 2
#define AK8963_HXL 0x03 //x axis low byte
#define AK8963_CNTL1 0x0A //mode choice
#define AK8963_CNTL2 0x0B //soft reset
#define AK8963_ASAX 0x10


void getAres();
void getGres();
void I2C_Init();
void Init_MPU_9250();
uint8_t check_for_mpu_9250();
uint8_t check_for_ak8963();
void get_data_from_mpu_9250(uint16_t *_temp, int16_t* _acc, int16_t* _gyr);
void get_real_values(uint16_t *_temp,int16_t* _acc, int16_t* _gyr, float *real_temp, float* real_acc, float* real_gyr);
#endif /* INC_I2C_H_ */
