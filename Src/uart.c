#include "stm32l0xx_hal.h"
#include "uart.h"

UART_HandleTypeDef huart1;

uint8_t rx_buffer[256];
uint8_t rx_index = 0;

struct rx_process_state_machine sm;

const uint8_t private_service_UUID[32] = "61aaba9617febe18d21813e84fd4026c";

const uint8_t actual_rpm_ch_adc_ch[32] = "9742d7b0226e12a69bae9970996e0016";
const uint8_t set_rpm_ch[32] = "d9e66c6a9cb7337f22d8406278b39193";
const uint8_t acc_gyr_mag_ch[32] = "7945e07891768d93740540171efc6b61";

extern uint16_t RPM;

const BT_Command RN4020_init[] =
{
		//{3,"+r\n"}, //enable echo: only works after restart
		{6, "SF,1\r\n"},//factory reset
		{6,"SB,4\r\n"}, //115200 baud rate
		{13,"SR,30000000\r\n"}, //features: auto advertise
		{18,"SN,Motor_Control\r\n"},
		{13,"SS,10000001\r\n"},//set services: health thermometer and user defined
		//{4,"LC\r\n"},//lists available client services and their characteristics
		//{4,"LS\r\n"},//lists available server services and their characteristics
		{4,"PZ\r\n"}, //clear current private characteristics
		{37,"PS,61aaba9617febe18d21813e84fd4026c\r\n"}, //define the private service
		{43,"PC,9742d7b0226e12a69bae9970996e0016,12,03\r\n"}, //actual rpm and motor current: readable and notify, 3bytes
		{43,"PC,d9e66c6a9cb7337f22d8406278b39193,08,01\r\n"}, //set rpm: writeable, 1byte 0-255
		{43,"PC,7945e07891768d93740540171efc6b61,12,12\r\n"}, //acc,gyr,mag data: readable and notify, 18 bytes
		//{43,"PC,4da280fa966eeceac1e11c6eaca2a8d1,12,06\r\n"}, //gyroscope data: readable and notify, 6bytes
		//{43,"PC,e3b5b7bf804fa8a08bc89aad72d276a5,12,06\r\n"}, //magnetometer data: readable and notify, 6bytes
		//{43,"PC,e62ae110876cbe3315109c77e86fb0c6,12,02\r\n"}, //adc motor data: readable and notify, 2 bytes
		{5,"R,1\r\n"}//reboot device

		//private service: the associated UUID must be 128 bits long (16 bytes)

		//Characteristic address command: S to address server services
		//Characteristic address command: C to address client services

		//a characteristic can be addressed either by its UUID or its handle
		//to address a characteristic by its UUID, the second letter of a characteristic access command is U
		//to address a characteristic by its handle, the second letter of a characteristic access command is H

		//read a characteristic: third letter is R
		//to write a characteristic: third letter is W

		//client service: access to a characteristic: 4th letter is V: value access request, C: configuration request

		//CHR: read client service characteristic, by addressing its handle
		//CHW: write client service characteristic, by addressing its handle, second parameter is the number
		//configuration handle writing: 0x0000: stops both below
		//								0x0001: starts notification
		//								0x0002: starts indication
		//CURC parameter UUID
		//CURV
		//CUWC 2nd parameter is 1(starts notification or indication) or 0(turns that off)
		//CUWV 2nd parameter is a hexadecimal value
		//SHR (reading the contents of a characteristic locally is always permitted)
		//SHW 2nd parameter is hexa
		//SUR
		//SUW

		//up to 10 private characteristic
		//Private characteristic configuration starts with P
		//PC: set private characteristic, must be called after PS (UUID has been set)
		//firmware 1.20 and 1.23: multiple private services are accessable on peer device
		//add private characteristic to private service, multiple characteristics are allowed
		//PC,UUID,property bitmap,max_data_size_in_bytes(<=20),(8 bit security flag) 20:0x14
		//PS: define private service by UUID
		//PZ: clears all private service and server characteristic settings
};
const BT_Command command_list[] =
{
	{4,"SUW,"},
	{4,"SUR,"},
	{1,","},
	{2,"\r\n"},
	{2,"LS"}
};
void UART_Init(void)
{
	__HAL_RCC_USART1_CLK_ENABLE();


	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	huart1.Init.OneBitSampling = UART_ONEBIT_SAMPLING_DISABLED;
	huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT;
	huart1.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
	HAL_UART_Init(&huart1);

	huart1.Instance->CR1 |= USART_CR1_RXNEIE;//enable RXNE interrupt
	HAL_NVIC_SetPriority(USART1_IRQn,0,0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
}


//const uint8_t actual_rpm_ch_handle[2] = "";

void init_RN4020()
{
	//CMD/MLDP HIGH PA1
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1,1);
	//WAKE_HW PA11, WAKE_SW PA8 be VDD
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 1);
	HAL_Delay(500);
	for(uint8_t i = 0;i<11;++i)
	{
		HAL_UART_Transmit(&huart1,RN4020_init[i].command,RN4020_init[i].size,1000);
		//while (HAL_UART_GetState(&huart1)!= HAL_UART_STATE_READY);
		HAL_Delay(100);
	}
	HAL_Delay(1500);

}
//although sending the data with hexa format, the required bytes parameter
//is the real length of the values. e.g. 16bit -> 2 bytes
//the length of the character array must be twice as long e.g. 4 bytes
void write_to_characteristic(uint8_t* UUID, uint8_t* value, uint8_t bytes)
{
	uint8_t values_in_hex[bytes<<1];//2*bytes
	write_to_hex_string(value,bytes,values_in_hex);
	HAL_UART_Transmit(&huart1,command_list[0].command,command_list[0].size,1000);
	HAL_UART_Transmit(&huart1,UUID,32,1000);
	HAL_UART_Transmit(&huart1,command_list[2].command,command_list[2].size,1000);
	HAL_UART_Transmit(&huart1,values_in_hex,bytes<<1,1000);
	HAL_UART_Transmit(&huart1,command_list[3].command,command_list[3].size,1000);
	//SUW,UUID,value
	HAL_Delay(2);
}
void read_from_characteristic(uint8_t* UUID, uint8_t* value, uint8_t bytes)
{
	uint8_t temp_values[bytes];
	huart1.Instance->CR1 &= ~USART_CR1_RXNEIE;//disable RXNE interrupt
	HAL_UART_Transmit(&huart1,command_list[1].command,command_list[1].size,1000);
	HAL_UART_Transmit(&huart1,UUID,32,1000);
	//while (HAL_UART_GetState(&huart1) != HAL_UART_STATE_READY);
	HAL_UART_Transmit(&huart1,command_list[3].command,command_list[3].size,1000);
	HAL_UART_Receive(&huart1,temp_values,bytes,1000);
	huart1.Instance->CR1 |= USART_CR1_RXNEIE;//enable RXNE interrupt

	for(uint8_t i = 0;i<bytes;++i)
	{
		value[i] = temp_values[i];
	}
}
void write_to_hex_string(uint8_t* values,uint8_t bytes, uint8_t* values_in_hex)
{
	uint8_t upper = 0;
	uint8_t bottom = 0;
	for(uint8_t i = 0; i<bytes; ++i)
	{
		 upper = values[i]>>4;
		 bottom = values[i]&0x0f;
		 if(upper>9)
			 values_in_hex[i<<1] = upper+55;//2*i
		 else
			 values_in_hex[i<<1] = upper+48;//2*i
		 if(bottom>9)
			 values_in_hex[i<<1|0x01] = bottom+55;//2*i+1
		 else
			 values_in_hex[i<<1|0x01] = bottom+48;//2*i+1
	}
}
void read_from_hex_string(uint8_t* values_in_hex,uint8_t characters,uint8_t* values)
{
	uint8_t temp = 0;
	for(uint8_t i = 0; i<(characters>>1); ++i)
	{
		if(values_in_hex[i<<1]<65)
			temp |= values_in_hex[i]-48;
		else
			temp |= values_in_hex[i]-55;
		temp = temp<<4;
		if(values_in_hex[i<<1|0x01]<65)
			temp |= values_in_hex[i<<1|0x01]-48;
		else
			temp |= values_in_hex[i<<1|0x01]-55;
		values[i] = temp;
	}
}
void process_rx_buffer()
{
	if(sm.process_rx_buffer_index!=rx_index)
	{
		switch(sm.rx_state)
		{
			case 0:
				if(rx_buffer[sm.process_rx_buffer_index] == 'W')
				{
					//if the next char is "V", we should wait til \r and then process the string
					sm.rx_state = 1;
				}
				else
				{
					sm.process_rx_buffer_start++;
				}
				break;
			case 1:
				//then next char is V, then receive the 16 bit handle and the data
				if(rx_buffer[sm.process_rx_buffer_index] == 'V')
					sm.rx_state = 2;
				else
				{
					//e.g. "WC"
					sm.process_rx_buffer_start = sm.process_rx_buffer_index+1;
					sm.rx_state = 0;
				}
				break;
			case 2:
				if(rx_buffer[sm.process_rx_buffer_index] == '\r')
				{
					//handle: 16 bit
					//values 1st index:
					//sm.process_rx_buffer_start W
					//sm.process_rx_buffer_start+1 V
					//sm.process_rx_buffer_start+2 ,
					//sm.process_rx_buffer_start+3 16b3
					//sm.process_rx_buffer_start+4 16b2
					//sm.process_rx_buffer_start+5 16b1
					//sm.process_rx_buffer_start+6 16b0
					//sm.process_rx_buffer_start+7 ,
					//sm.process_rx_buffer_start+8 1st char
					//...
					//values last index: sm.process_rx_buffer_index-1
					//which handle?
					//RPM_set and ota are possible now
					uint8_t rpm_handle[4] = "0014";
					if(!Buffercmp(&rx_buffer[sm.process_rx_buffer_start+3],rpm_handle,4))
					{
					//read data
						uint8_t set_rpm;
						uint8_t chars = sm.process_rx_buffer_index-sm.process_rx_buffer_start-8;
						read_from_hex_string(&rx_buffer[sm.process_rx_buffer_start+8],chars,&set_rpm);
						//process character arrays: handle and value
						//set RPM value
						RPM = set_rpm;
					}
					sm.rx_state = 0;
					sm.process_rx_buffer_start = sm.process_rx_buffer_index+1;
				}
				else
				{
					//wait for the next character, nothing to do here
				}
				break;
		}
		sm.process_rx_buffer_index++;

	}
}
void set_baudrate(uint32_t br)
{
	huart1.Init.BaudRate = br;
	HAL_UART_Init(&huart1);
}
void list_server_services()
{
	HAL_UART_Transmit(&huart1,"LS\r\n",4,1000);
}
/*
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

}
*/
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
  while (BufferLength--)
  {
    if ((*pBuffer1) != *pBuffer2)
    {
      return BufferLength;
    }
    pBuffer1++;
    pBuffer2++;
  }

  return 0;
}
