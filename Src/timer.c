#include "stm32l0xx.h"
#include "stm32l0xx_hal.h"
#include "timer.h"
#include "uart.h"
#include "adc.h"

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim21;
TIM_HandleTypeDef htim22;

uint8_t i = 0;
uint32_t t = 0;

uint8_t sw_1kHz = 0;
uint16_t sw_1Hz = 0;


extern uint16_t RPM;
extern float act_RPM;

extern uint8_t sw_1Hz_interrupt_flag;

float next_kitoltesi_tenyezo = 0;
float integral = 0;

//averaged values
uint16_t ICValue_F_t[16];

uint8_t gomb1;
uint8_t gomb2;
uint8_t gomb3;
uint8_t gomb_p = 0x00;//bin�ris �llapotokat tartalmaz� v�ltoz�

//extern uint32_t adc[3];
//extern ADC_HandleTypeDef hadc;


//20Khz-es PWM a motor hajt�s�ra
void Timer2_Init()
{
  __TIM2_CLK_ENABLE();
  TIM_OC_InitTypeDef sConfigOC;
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = period_value-1;//1599
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.State = HAL_TIM_STATE_RESET;

  HAL_TIM_Base_Init(&htim2);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig);

  HAL_TIM_PWM_Init(&htim2);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);//pwm configolva

  HAL_TIM_Base_Start(&htim2);

}
//input capture 2 csatorn�n, ez alapj�n forg�sir�ny meghat�roz�sa is
void Timer21_Init()
{
  HAL_Delay(1000);//
  __TIM21_CLK_ENABLE();

  TIM_IC_InitTypeDef sConfigIC;
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim21.Instance = TIM21;
  htim21.Init.Prescaler = 31;
  htim21.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim21.Init.Period = 0xFFFF;
  htim21.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim22.State = HAL_TIM_STATE_RESET;

  HAL_TIM_Base_Init(&htim21);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim21, &sClockSourceConfig);

  HAL_TIM_IC_Init(&htim21);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim21, &sMasterConfig);

  sConfigIC.ICPolarity = TIM_ICPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 3;
  HAL_TIM_IC_ConfigChannel(&htim21, &sConfigIC, TIM_CHANNEL_1);

 //sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
 // HAL_TIM_IC_ConfigChannel(&htim21, &sConfigIC, TIM_CHANNEL_2);

  HAL_NVIC_SetPriority(TIM21_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(TIM21_IRQn);

}
void start_motor()
{
	unsigned int i=0;
	HAL_GPIO_WritePin(n_sleep_motor_GPIO_Port, n_sleep_motor_Pin,1);//wake up
	//wait 2 ms before applying pwm signals
	HAL_Delay(2);
	HAL_GPIO_WritePin(dir_motor_GPIO_Port, dir_motor_Pin,1);
	SetPWM(0);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	//HAL_GPIO_WritePin(pwm_motor_GPIO_Port, pwm_motor_Pin,1);
	HAL_TIM_IC_Start_IT(&htim21, TIM_CHANNEL_2);
    HAL_TIM_IC_Start_IT(&htim21, TIM_CHANNEL_1);
    gomb_p|=0x8; //set bit
}
void stop_motor()
{
	HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
	HAL_GPIO_WritePin(n_sleep_motor_GPIO_Port, n_sleep_motor_Pin, 0);//go to sleep
	HAL_TIM_IC_Stop_IT(&htim21, TIM_CHANNEL_2); // stop input capture
	HAL_TIM_IC_Stop_IT(&htim21, TIM_CHANNEL_1);
	__HAL_TIM_SET_COMPARE(&htim21, TIM_CHANNEL_2, 0xFFFF); //set max value for time period
	__HAL_TIM_SET_COMPARE(&htim21, TIM_CHANNEL_1, 0xFFFF);
	gomb_p&=0xf7; //clear bit
	//frequency = 0;
}
void SetPWM(uint16_t d)
{
	htim2.Instance->CCR1 = d;
}
//20kHz-es frekvenci�j� �ltal�nos timer, a gombnyom�sokra
void Timer22_Init()
{
  __TIM22_CLK_ENABLE();

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim22.Instance = TIM22;
  htim22.Init.Prescaler = 0;
  htim22.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim22.Init.Period = period_value;
  htim22.State = HAL_TIM_STATE_RESET;

  HAL_TIM_Base_Init(&htim22);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim22, &sClockSourceConfig);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim22, &sMasterConfig);

  HAL_NVIC_SetPriority(TIM22_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(TIM22_IRQn);

  HAL_TIM_Base_Start_IT(&htim22);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef * htim)
{
	if (htim->Instance == TIM22)
	{
		Timer22Handler();
	}
}
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef* htim)
{
	uint32_t ic_f = 0;
	uint16_t ICValue_F;

	if(htim->Instance == TIM21 && HAL_TIM_ACTIVE_CHANNEL_1)
	{

		//ICValue_t[i] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
		ICValue_F_t[i] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
		++i; if(i == 16) i = 0;
		for(uint8_t j=0;j<16;++j)
		{
			//ic_r += ICValue_t[j];
			ic_f += ICValue_F_t[j];
		}
		//ICValue = ic_r>>4;
		ICValue_F = ic_f>>4;
		act_RPM = RPM_to_freq*magic_number/(float)(1.0+htim->Instance->PSC)/(float)(ICValue_F);
		//count = __HAL_TIM_GET_COUNTER(&htim21);
		htim->Instance->CNT = 0;
	}
}

//gombok kezel�s�re handler fv
void Timer22Handler()
{
	uint8_t temp1 = HAL_GPIO_ReadPin(gomb1_GPIO_Port, gomb1_Pin);
	uint8_t temp2 = HAL_GPIO_ReadPin(gomb2_GPIO_Port, gomb2_Pin);
	uint8_t temp3 = HAL_GPIO_ReadPin(gomb3_GPIO_Port, gomb3_Pin);

	gomb1=(gomb1<<1)|temp1;
	gomb2=(gomb2<<1)|temp2;
	gomb3=(gomb3<<1)|temp3;

	if(gomb1 == 0xff)//el van engedve
	{
		gomb_p|=0x01;
	}
	if(gomb1 == 0x00 && (gomb_p&0x01))// meg lett nyomva
	{
		gomb_p&=0xfe;//als� bit 0-zva
		//do what u want if you press the button 1
		//CMD/MLDP m�d k�z�tt v�lt�s
		//HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_1);
		//increase speed

		if (RPM<max_RPM && RPM<=0.95*max_RPM)
		{
			RPM+=0.05*max_RPM;
		}

	}

	if(gomb2 == 0xff)//el van engedve
	{
		gomb_p|=0x02;
	}
	if(gomb2 == 0x00 && (gomb_p&0x02))// meg lett nyomva
	{
		gomb_p&=0xfd;//als� felett 1 bit 0-zva
		//do what u want if you press the button 2

		// toggle wake_sw pin
		//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
		//decrease speed

		if (RPM>0 && RPM>=0.05*max_RPM)
		{
			RPM-=0.05*max_RPM;
		}
	}

	if(gomb3 == 0xff)//el van engedve
	{
		gomb_p|=0x04;
	}
	if(gomb3 == 0x00 && (gomb_p&0x04))// meg lett nyomva
	{
		gomb_p&=0xfb;//alulr�l a 3. bit 0-zva
		//do what u want if you press the button 3


		//motor ki-be kapcsol�sa

		if(gomb_p&0x8) //alulr�l a 4. bit 1
			stop_motor(); // ez be is �ll�tja a 0-ra
		else
			start_motor();
	}

	++sw_1kHz;
	if(sw_1kHz == 20)
	{
		sw_1kHz = 0;

		//1Khz-es software interrupt; motor szab�lyoz� fv h�v�sa
		SetMotor(RPM);
	}
	++sw_1Hz;
	if(sw_1Hz == 20000)
	{
		sw_1Hz = 0;
		sw_1Hz_interrupt_flag = 1;
		HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
	}
	//adc channel 9
	//adc[0] = HAL_ADC_GetValue(&hadc);
}
//set motor speed in RPM
void SetMotor(float speed)
{
	 // ha �ll a motor, akkor nem fut az input capture, ekkor a gomb_p alulr�l a 4. bit �rt�ke 0
	 if(!(gomb_p & 0x08))
		 return;

	 float error = speed - act_RPM;
	 integral += error;
	 //derival = error-error_p;

	 next_kitoltesi_tenyezo += (error/121906.0 + integral/74498050.0);
	 if(next_kitoltesi_tenyezo<0)
	 {
		 integral=0;
		 error=0;
		 next_kitoltesi_tenyezo = 0;
	 }
	 else if(next_kitoltesi_tenyezo>1)
	 {

		 next_kitoltesi_tenyezo = 1;
		 integral=0;
		 error=0;
	 }
	SetPWM((uint16_t)period_value*next_kitoltesi_tenyezo);

}
