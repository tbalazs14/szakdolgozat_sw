#include "stm32l0xx_hal.h"
#include "adc.h"
ADC_HandleTypeDef AdcHandle;
ADC_ChannelConfTypeDef        sConfig;
/* ADC init function */
extern uint16_t adc_value;
uint8_t conv_complete;
const uint32_t vref_int_cal = 0x1ff80078; //0x1ff80079

void ADC_Init()
{
	__HAL_RCC_ADC1_CLK_ENABLE();

	AdcHandle.Instance = ADC1;

	AdcHandle.Init.OversamplingMode      = DISABLE;
	AdcHandle.Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV2;
	AdcHandle.Init.LowPowerAutoPowerOff  = ENABLE;
	AdcHandle.Init.LowPowerFrequencyMode = DISABLE;
	AdcHandle.Init.LowPowerAutoWait      = DISABLE;

	AdcHandle.Init.Resolution            = ADC_RESOLUTION_12B;
	AdcHandle.Init.SamplingTime          = ADC_SAMPLETIME_71CYCLES_5;
	AdcHandle.Init.ScanConvMode          = ADC_SCAN_DIRECTION_FORWARD;
	AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
	AdcHandle.Init.ContinuousConvMode    = ENABLE;
	AdcHandle.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
	AdcHandle.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
	AdcHandle.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
	AdcHandle.Init.DMAContinuousRequests = DISABLE;

	/* Initialize ADC peripheral according to the passed parameters */
	if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
	{
		while(1);
	}

	if (HAL_ADCEx_Calibration_Start(&AdcHandle, ADC_SINGLE_ENDED) != HAL_OK)
	{
		while(1);
	}

	//a motorvez�rl� �ltal a Sense ellen�ll�ssal a felvett �rammal ar�nyos fesz�lts�g
	//VPROPI = 5*I
	//sz�rve van egy RC taggal, ahol R=100k �s C=1nF
	sConfig.Channel = ADC_CHANNEL_9;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	{
		while(1);
	}
	sConfig.Channel = ADC_CHANNEL_VREFINT;
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	{
		while(1);
	}

	//HAL_ADC_Start(&AdcHandle);
	HAL_NVIC_SetPriority(ADC1_COMP_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(ADC1_COMP_IRQn);


}
uint16_t ADC_Get_Value(uint32_t channel)
{
	conv_complete = 0;
	AdcHandle.Instance->CHSELR = channel & ADC_CHANNEL_MASK;
	if (HAL_ADC_Start_IT(&AdcHandle) != HAL_OK)
	{
		while(1);
	}
	while(!conv_complete);
	if(HAL_ADC_Stop_IT(&AdcHandle)!= HAL_OK)
	{
		while(1);
	}
	return adc_value;
}

uint32_t ADC_Get_Voltage(uint32_t channel)
{
	uint16_t adc_v = ADC_Get_Value(channel);
	uint32_t voltage;
	uint16_t vrefint_data = ADC_Get_Value(ADC_CHANNEL_VREFINT);
	//bandgap reference voltage kb. 1,25V very nearly constant -> VDD
	//~1500 vrefint_data 1,25V, full scale: 4095 vdd, cheating: dividing with 4096 is shifting right by 12
	//vdd = 1,25V*full scale/vrefint:data
	//1,25V: *(vref_int_cal)
	voltage = (vref_int_cal*3000/(uint32_t)vrefint_data)>>12;
	return voltage;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
  // Get the converted value of regular channel
	adc_value = (uint16_t)HAL_ADC_GetValue(AdcHandle);
	conv_complete = 1;
}
