#include "stm32l0xx_hal.h"
#include "gpio.h"

extern uint8_t i2c_data_ready_interrupt_flag;

void Init_AF()
{
	  GPIO_InitTypeDef GPIO_InitStruct;
	  //I2C gpio inicializ�l�s:
	  //SCL & SDA
	  GPIO_InitStruct.Pin = i2c_scl_Pin | i2c_sda_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  GPIO_InitStruct.Alternate = GPIO_AF1_I2C1;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  //UART gpio inicializ�l�s:
	  //UART_TX & UART_RX
	  GPIO_InitStruct.Pin = uart_tx_Pin | uart_rx_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	  GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
	  HAL_GPIO_Init(uart_tx_GPIO_Port, &GPIO_InitStruct);

	  //timer inicializ�l�s: k�t hall szenzor �s egy pwm kimenet
	  //k�t hall szenzor �ltal m�rt jel k�z�tti f�zisk�l�nbs�gb�l kider�thet� az ir�ny
	  //HALL A fordulatsz�m m�r�shez
	  GPIO_InitStruct.Pin = hall_a_Pin | hall_b_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	  GPIO_InitStruct.Alternate = GPIO_AF0_TIM21;
	  HAL_GPIO_Init(hall_a_GPIO_Port, &GPIO_InitStruct);

	  //pwm
	  GPIO_InitStruct.Pin = pwm_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	  GPIO_InitStruct.Alternate = GPIO_AF5_TIM2;
	  HAL_GPIO_Init(pwm_motor_GPIO_Port, &GPIO_InitStruct);
}

void Init_Analog()
{
	  GPIO_InitTypeDef GPIO_InitStruct;
	  /*
	  Anal�g bemenetek: PA9: motor �ram�val egyenes ar�nyos fesz�lts�g. max 2.5V

	  /*Configure GPIO pin : motor_current_Pin */
	  GPIO_InitStruct.Pin = motor_current_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	  HAL_GPIO_Init(motor_current_GPIO_Port, &GPIO_InitStruct);


}

void GPIO_Init()
{
	  GPIO_InitTypeDef GPIO_InitStruct;

	  //PA0: 0 when 12VDC is applied
	  //	 1 when 4-6VB is applied and boost converter is active
	  GPIO_InitStruct.Pin = GPIO_PIN_0;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  //Bluetooth pin CMD/MLDP
	  GPIO_InitStruct.Pin = GPIO_PIN_1;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  //Bluetooth WAKE_SW & WAKE_HW, internal pull down in RN4020
	  //always in MLDP mode, CMD/MLDP is GND
	  GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_11;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  /*Configure GPIO pin : n_sleep_motor_Pin */
	  GPIO_InitStruct.Pin = n_sleep_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(n_sleep_motor_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : dir_motor_Pin */
	  GPIO_InitStruct.Pin = dir_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(dir_motor_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : slow_fast_brake_motor_Pin */
	  GPIO_InitStruct.Pin = slow_fast_brake_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(slow_fast_brake_motor_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : sw_wake_motor_Pin */
	  GPIO_InitStruct.Pin = sw_wake_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(sw_wake_motor_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : hw_wake_motor_Pin */
	  GPIO_InitStruct.Pin = hw_wake_motor_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(hw_wake_motor_GPIO_Port, &GPIO_InitStruct);

	  /*Magnetom�ter interrupt pin */
	  GPIO_InitStruct.Pin = int_mag_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	  //it would be very fast, it is not needed, 1Hz is more than enough for updating sensor data
	  //MPU-9250 interrupt pin: PB5
	  GPIO_InitStruct.Pin = GPIO_PIN_5;
	  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  //set external interrupt priority and enable the interrupt
	  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 2, 0);
	  //HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
	  HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);

	  // Gombok inicializ�l�sa:
	  GPIO_InitStruct.Pin = gomb1_Pin|gomb2_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(gomb1_GPIO_Port, &GPIO_InitStruct);

	  GPIO_InitStruct.Pin = gomb3_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(gomb3_GPIO_Port, &GPIO_InitStruct);

}
void init_all_pins()
{
	  __GPIOA_CLK_ENABLE();
	  __GPIOB_CLK_ENABLE();
	  GPIO_Init();
	  Init_AF();
	  Init_Analog();
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_5)
	{
		i2c_data_ready_interrupt_flag = 1;
	}
	HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);
}

