#include "stm32l0xx_hal.h"
#include "adc.h"
#include "gpio.h"
#include "hardware_init.h"
#include "i2c.h"
#include "mxconstants.h"
#include "uart.h"
#include "timer.h"
/********************************************************
GLOBAL VARIABLES
**********************************************************/

//motor: actual speed and setpoint
float act_RPM;
uint16_t RPM = 60;

//motor sense voltage
uint16_t adc_value;

//contain i2c measurement data
uint16_t temp;
int16_t acc[3];
int16_t gyr[3];

//timer flags
uint8_t sw_1Hz_interrupt_flag;
uint8_t i2c_data_ready_interrupt_flag;


//bluetooth characteristic channels
//extern uint8_t set_rpm_ch[32];
extern uint8_t acc_gyr_mag_ch[32];
extern uint8_t actual_rpm_ch_adc_ch[32];


uint32_t temp_var;
uint8_t set_RPM_from_client[6];

int main()
{
	//i2c measurement data in degree C, g and degree/s
	float temperature;
	float acc_x_y_z[3];
	float gyr_x_y_z[3];

	//i2c sensor present?
	uint8_t is_mpu_9250_present = 0;

	//motor temporary data
	uint16_t motor_current;
	uint8_t motor_current_8_act_RPM[3];

	//accelerometer and gyroscope temporary data
	uint8_t acc_8_gyr_8[18];

	//bluetooth RPM data received over uart
	uint8_t set_rpm[2];

	__initialize_hardware();
	HAL_Delay(500);
	init_all_pins();

	UART_Init();
	init_RN4020();

	list_server_services();

	ADC_Init();
	I2C_Init();
	Timer2_Init();
	Timer21_Init();
	Timer22_Init();
	start_motor();
	RPM = 0;

	is_mpu_9250_present = check_for_mpu_9250();
	if(is_mpu_9250_present)
		Init_MPU_9250();

	while (1)
	{
		process_rx_buffer();
		if(sw_1Hz_interrupt_flag)
		{
			sw_1Hz_interrupt_flag = 0;

			//send motor current (read through sense resistor)
			//send motor rpm over bluetooth
			motor_current = (uint16_t)ADC_Get_Voltage(ADC_CHANNEL_9);
			motor_current_8_act_RPM[0] = adc_value>>8;
			motor_current_8_act_RPM[1] = adc_value&0xff;
			motor_current_8_act_RPM[2] = (uint8_t)act_RPM;
			//motor_current_8_act_RPM[3] = (uint16_t)act_RPM&0xff;
			write_to_characteristic(actual_rpm_ch_adc_ch,motor_current_8_act_RPM,3);


			//read user set RPM from bluetooth
			//read_from_characteristic(set_rpm_ch, set_RPM_from_client, 6);
			//read_from_hex_string(set_RPM_from_client,4,set_rpm);

		}
		if(i2c_data_ready_interrupt_flag)
		{
			i2c_data_ready_interrupt_flag = 0;

			//send accelerometer and gyroscope raw data over bluetooth
			get_data_from_mpu_9250(&temp,acc,gyr);
			get_real_values(&temp,acc,gyr,&temperature,acc_x_y_z,gyr_x_y_z);

			for(uint8_t i = 0; i<3; ++i)
			{
				acc_8_gyr_8[i<<1] = acc[i]>>8;//2*i
				acc_8_gyr_8[i<<1|0x01] = acc[i]&0xff;//2*i+1
				acc_8_gyr_8[6+i<<1] = gyr[i]>>8;
				acc_8_gyr_8[6+i<<1|0x01] = gyr[i]&0xff;
			}
			write_to_characteristic(acc_gyr_mag_ch,acc_8_gyr_8,18);

		}
	}

}
