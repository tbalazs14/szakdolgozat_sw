#include "stm32l0xx_hal.h"
#include "i2c.h"

I2C_HandleTypeDef hi2c1;
uint8_t i2c_tx_buffer[16];
uint8_t i2c_rx_buffer[16];
//uint8_t mpu9250_detected = 0;

//address 0x3b-0x48: sensor data
//acc_xout_h
//acc_xout_l
//acc_yout_h
//acc_yout_l
//acc_zout_h
//acc_zout_l
//temp_out_h
//temp_out_l
//gyro_xout_h
//gyro_xout_l
//gyro_yout_h
//gyro_yout_l
//gyro_zout_h
//gyro_zout_l


//MEMS sensor: accelerometer, gyroscope, magnetometer
//MPU-9250
//who_am_i register: address 0x75, default value: 0x71
//MPU-6050 could be compatible
//7 bit address: b110100X -> 2 mpu9250s can be on the bus, X: AD0 level (on the module it is GND)
//1. read WHO_AM_I register
//2. self test
//3. calibration
//
//Magnetometer in MPU-9250: AK8963
//Device ID is read-only: 0x00 WIA value 0b01001000 7 bit it would be 0x48, but it will be 0x90
//0x01: INFO read only
//0x02: ST1 read only
//0x01: INFO read only
//0x01: INFO read only
//0x01: INFO read only
//0x03: HXL
//0x04: HXH
//0x05: HYL
//0x06: HYH
//0x07: HZL
//0x08: HZH
//accelerometer and gyroscope contain measurement data in big endian format
//while magnetometer contains measurement data in little endian
//16 bit int -32768<data<32767
//-4912uT<data<4912uT
//sensitivity adjustment Hadj = H*((ASA-128)/256+1)
//0x10 ASAX
//0x11 ASAY
//0x12 ASAZ
//



//accelerometer: unit is g
enum Ascale
{
	AFS_2G = 0,
	AFS_4G,
	AFS_8G,
	AFS_16G
};

//unit is degree/s
enum Gscale
{
	GFS_250DPS = 0,
	GFS_500DPS,
	GFS_1000DPS,
	GFS_2000DPS
};


//unit is microstesla
enum Mscale
{
	MFS_14BITS = 0,//LSB = 0.6mT
	MFS_16BITS//LSB = 0.15mT
};
uint8_t Ascale = AFS_2G;
uint8_t Gscale = GFS_250DPS;
uint8_t Mscale = MFS_16BITS;

float aRes, gRes, mRes;

void getAres()
{
	switch(Ascale)
	{
		case AFS_2G:
			aRes = 2.0/32768.0;
			break;
		case AFS_4G:
			aRes = 4.0/32768.0;
			break;
		case AFS_8G:
	          aRes = 8.0/32768.0;
	          break;
	    case AFS_16G:
	          aRes = 16.0/32768.0;
	          break;
	}
}
//gyroscope: unit is degree per second
//full scale
//S=sensitivity changes according to full scale
void getGres()
{
	switch (Gscale)
	{
	  case GFS_250DPS:
			gRes = 250.0/32768.0;//S=131LSB/DPS
			break;
	  case GFS_500DPS:
			gRes = 500.0/32768.0;//S=65.5LSB/DPS
			break;
	  case GFS_1000DPS:
			gRes = 1000.0/32768.0;//S=32.8LSB/DPS
			break;
	  case GFS_2000DPS:
			gRes = 2000.0/32768.0;//S=16.4LSB/DPS
			break;
	}
}
//scale to milliGauss
void getMres()
{
	switch(Mscale)
	{
		case MFS_14BITS:
			mRes = 10.0*4912.0/8190.0;
			break;
		case MFS_16BITS:
			mRes = 10.0*4912.0/32760.0;
			break;
	}
}


void I2C_Init()
{
	__I2C1_CLK_ENABLE();

	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = I2C_TIMING;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
	HAL_I2C_Init(&hi2c1);
	/**Configure Analogue filter
	*/
	HAL_I2CEx_AnalogFilter_Config(&hi2c1, I2C_ANALOGFILTER_ENABLED);

	//enable i2c interrupt
	HAL_NVIC_SetPriority(I2C1_IRQn,1,0);
	HAL_NVIC_EnableIRQ(I2C1_IRQn);
	getAres();
	getGres();
}
uint8_t check_for_ak8963()
{
	uint8_t who_am_i = AK8963_WHO_AM_I;
	uint8_t data = 0;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,&who_am_i,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	//receive data from ak8963
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_AK8963,&data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	if(data==0x48)
		return 1;
	else
		return 0;
}
//if mpu9250 is present, then the function will return 1;
uint8_t check_for_mpu_9250()
{
	uint8_t who_am_i=MPU_9250_WHO_AM_I;
	uint8_t data=0;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,&who_am_i,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	//receive data from mpu_9250
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,&data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	if(data==0x71)
		return 1;
	else
		return 0;
}
//mpu-9250 magnetometer init and configuration
void Init_AK8963(float* adjustment_values)
{
	uint8_t measurement_mode = 0x02; //2 for 8Hz, 6 for 100Hz
	uint8_t i2c_data[2];
	uint8_t rx_data[3];
	//first, power down
	*(i2c_data) = AK8963_CNTL1;
	*(i2c_data+1) = 0x00;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,i2c_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_Delay(1);//wait 1ms before entering another mode
	//set AK8963_CNTL1 register to fuse access mode
	//then read the adjustment values
	*(i2c_data+1) = 0x0F;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,i2c_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	*(i2c_data) = AK8963_ASAX;
	for(uint8_t i = 0;i<3;++i)
	{
		HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,i2c_data,1);
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
		*(i2c_data) = *(i2c_data)+1;
		HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_AK8963,rx_data+i,1);
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
		adjustment_values[i] =  (float)(rx_data[i] - 128)/256.0 + 1.0;
	}
	//power down magnetometer
	*(i2c_data) = AK8963_CNTL1;
	*(i2c_data+1) = 0x00;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,i2c_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_Delay(1);

	// Configure the magnetometer for continuous read and highest resolution
	// set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
	// Set magnetometer data resolution and sample ODR
	*(i2c_data+1) =  Mscale << 4 | measurement_mode;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,i2c_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
}
void readMagData(int16_t * mag)
{
	uint8_t rx_data[7];  // x/y/z mag register data, ST2 register stored here, must read ST2 at end of data acquisition
	//read status register, if st1&0x01 == 1->data ready
	uint8_t address = AK8963_ST1;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,address,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_AK8963,rx_data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	if(rx_data[0]&0x01)
	{
		address = AK8963_HXL;
		for(uint8_t i=0;i<7;++i)
		{
			HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_AK8963,address++,1);
			while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
			HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_AK8963,rx_data+i,1);
			while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
		}
		if(!(rx_data[6]&0x80)) //HOFL bit is 0: measurement data is correct
		{
			mag[0] = ((int16_t)rx_data[1]<<8)|rx_data[0];
			mag[1] = ((int16_t)rx_data[3]<<8)|rx_data[2];
			mag[2] = ((int16_t)rx_data[5]<<8)|rx_data[4];
		}

	}
}
//mpu9250 accelerometer and gyroscope init and configuration
void Init_MPU_9250()
{
	uint8_t tx_data[2];
	*(tx_data)= MPU_9250_PWR_MGMT1;
	*(tx_data+1) = 0x00;
	//waking up device: clear sleep bit(6) and enable all sensors
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_Delay(100);
	//select clock source to be PLL gyroscope
	*(tx_data+1)=0x01;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	//Configure gyroscope and accelerometer
	//Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
	//minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
	//be higher than 1 / 0.0059 = 170 Hz
	//DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
	//digital low pass filter
	//With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz

	//configuration register 0x03
	*(tx_data) = MPU_9250_CONFIG;
	*(tx_data+1) = 0x03;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	//set sample rate = gyroscope output rate/(1+MPU_9250_SMPLRT_DIV_REG)
	//200Hz
	*(tx_data) = MPU_9250_SMPLRT_DIV;
	*(tx_data+1) = 0x04;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	// Set gyroscope full scale range
	// Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
	*(tx_data) = MPU_9250_GYRO_CONFIG;
	//read GYRO_CONFIG register value
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data+1,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	*(tx_data+1) = *(tx_data+1) & 0xE4; //clear fchoice bits [1:0] and full scale select bits[4:3]
	*(tx_data+1) = *(tx_data+1) | Gscale << 3; // Set full scale range for the gyro
	//write GYRO_CONFIG register with the new data
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	// Set accelerometer full-scale range configuration
	//current ACCEL_CONFIG
	*(tx_data) = MPU_9250_ACC_CONFIG;

	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data+1,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	*(tx_data+1) = *(tx_data+1) & 0xE7; //clear full scale select bits[4:3]
	*(tx_data+1) = *(tx_data+1) | Ascale << 3; // Set full scale range for the accelerometer

	// Write new ACCEL_CONFIG register with the new data
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);


	// Set accelerometer sample rate configuration
	// It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
	// accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
	*(tx_data) = MPU_9250_ACC_CONFIG_2;

	// get current ACCEL_CONFIG2 register value
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data+1,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	*(tx_data+1) = *(tx_data+1) & 0xF0; // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
	*(tx_data+1) = *(tx_data+1) | 0x03;  // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz

	// Write new ACCEL_CONFIG2 register value
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	// The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
	// but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

	//i2c master control: interrupt will still occur, even if external sensor is not available
	*(tx_data) = MPU_9250_I2C_MST_CTRL;
	*(tx_data+1) = 0x00;
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	*(tx_data) = MPU_9250_INT_PIN_CFG;
	*(tx_data+1) = 0x00; //enable interrupt, interrupt status register must be read to clear
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	*(tx_data) = MPU_9250_INT_ENABLE;
	*(tx_data+1) = 0x01; //enable raw data ready interrupt
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,tx_data,2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

}
void get_data_from_mpu_9250(uint16_t* _temp, int16_t* _acc, int16_t* _gyr)
{
	uint8_t raw_data[14];
	uint8_t memory_address = MPU_9250_ACCEL_XOUT_H;
	//data is ready to be read from the mpu_9250
	*(raw_data) = MPU_9250_INT_STATUS;
	*(raw_data+1) = 0x00;
	//read the interrupt status register, if fifo_overflow_int->data is 0x10
	//if raw sensor data is ready->data is 0x01
	HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,raw_data,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
	HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,raw_data+1,1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);

	if(raw_data[1]&0x01)
	{
		//read all registers
		for(uint8_t i=0;i<14;++i)
		{
			//send address
			HAL_I2C_Master_Transmit_IT(&hi2c1,I2C_ADDRESS_MPU_9250,&memory_address,1);
			while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
			//receive data from mpu_9250
			HAL_I2C_Master_Receive_IT(&hi2c1,I2C_ADDRESS_MPU_9250,raw_data+i,1);
			while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY);
			++memory_address;
		}
		_acc[0] = ((int16_t)raw_data[0]<<8)|raw_data[1];
		_acc[1] = ((int16_t)raw_data[2]<<8)|raw_data[3];
		_acc[2] = ((int16_t)raw_data[4]<<8)|raw_data[5];
		*_temp = ((uint16_t)raw_data[6]<<8)|raw_data[7];
		_gyr[0] =((int16_t)raw_data[8]<<8)|raw_data[9];
		_gyr[1] =((int16_t)raw_data[10]<<8)|raw_data[11];
		_gyr[2] =((int16_t)raw_data[12]<<8)|raw_data[13];

	}

}
void get_real_values(uint16_t *_temp,int16_t* _acc, int16_t* _gyr, float *real_temp, float* real_acc, float* real_gyr)
{
	*real_temp = (float)*_temp/333.87+21.0;
	real_acc[0] = (float)_acc[0]*aRes;
	real_acc[1] = (float)_acc[1]*aRes;
	real_acc[2] = (float)_acc[2]*aRes;
	real_gyr[0] = (float)_gyr[0]*gRes;
	real_gyr[1] = (float)_gyr[1]*gRes;
	real_gyr[2] = (float)_gyr[2]*gRes;
}
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{

}
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{

}
